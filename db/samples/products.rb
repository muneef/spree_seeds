require 'csv'

# Spree::SpreeFunctionaryDemo.load_sample('tax_categories')
# Spree::SpreeFunctionaryDemo.load_sample('shipping_categories')
# Spree::SpreeFunctionaryDemo.load_sample('option_types')
Spree::Sample.load_sample('taxons')

Spree::Sample.load_sample('stores')

default_shipping_category = Spree::ShippingCategory.find_by!(name: 'Default')
food_tax_category = Spree::TaxCategory.find_by!(name: 'Food')

Spree::Config[:currency] = 'INR'

# weight = Spree::OptionType.find_by!(name: 'weight')
# length = Spree::OptionType.find_by!(name: 'length')
# size = Spree::OptionType.find_by!(name: 'size')

PRODUCTS = CSV.read(File.join(__dir__, 'variants.csv')).map do |(parent_name, taxon_name, product_name, tag, price, desc)|
  [parent_name, taxon_name, product_name, tag, price, desc]
end.uniq

PRODUCTS.each do |(parent_name, taxon_name, product_name, tag, price, desc)|  

  if(parent_name) 
    parent = Spree::Taxon.find_by!(name: parent_name)
    
    puts "parent 🚚"
    # puts parent
       
    # taxon = parent.children.find_by!(name: taxon_name)
    
    Spree::Product.where(name: product_name.titleize).first_or_create! do |product|
      product.price = price
      product.description = desc
      product.available_on = Time.zone.now
      # product.option_types = [weight]
      product.shipping_category = default_shipping_category
      product.tax_category = food_tax_category

      puts "🚚 [#{taxon_name}]"

      if(taxon_name && taxon_name.length > 1)

        # puts "🏖️"
        # puts taxon_name
        # taxon = parent.children.find_by!(name: taxon_name)
        puts "SHITS CREEK"
        product.taxons << "a"
        # product.taxons << taxon unless product.taxons.include?(taxon)
        product.sku = [taxon_name.delete(' '), product_name.delete(' '), product.price].join('_')
      else
        puts "🏖️🏖️"
      end
      
      # product.stores = Spree::Store.all
    end
  end
end

# Spree::Taxon.where(name: ['Bestsellers', 'New', 'Trending']).each do |taxon|
#   next if taxon.products.any?

#   taxon.products << Spree::Product.all.sample(30)
# end
