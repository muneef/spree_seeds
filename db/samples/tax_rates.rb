Spree::Sample.load_sample('tax_categories')
Spree::Sample.load_sample('zones')

karnataka = Spree::Zone.find_by!(name: 'Karnataka')
food = Spree::TaxCategory.find_by!(name: 'Food')

Spree::TaxRate.where(
  name: 'Karnataka',
  zone: karnataka,
  amount: 0.1,
  tax_category: food
).first_or_create! do |tax_rate|
  tax_rate.calculator = Spree::Calculator::DefaultTax.create!
end
