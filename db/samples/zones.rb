blr_zone = Spree::Zone.where(name: 'Karnataka', description: 'Bengaluru (Karnataka) Tax Zone', kind: 'state').first_or_create!
blr_state = Spree::Country.find_by!(iso3: 'IND').states.find_by(abbr: 'KA')
blr_zone.zone_members.where(zoneable: blr_state).first_or_create!
