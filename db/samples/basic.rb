# default_store = Spree::Store.default
# default_store.checkout_zone = Spree::Zone.find_by(name: 'Karnataka')
# default_store.default_country = Spree::Country.find_by(iso: 'IN')
# default_store.default_currency = 'INR'
# default_store.supported_currencies = 'INR'
# default_store.supported_locales = 'en'
# # default_store.logo = 'logo.svg'
# default_store.url = Rails.env.development? ? 'localhost:3000' : 'olivebee.co'
# default_store.mail_from_address = 'support@olivbee.co'
# default_store.name = 'OliveBee Organic Store'
# default_store.save!



# # add default stock of 1 to all products
# Spree::Variant.all.each do |variant|
#   next if variant.is_master? && variant.product.has_variants?

#   variant.stock_items.each do |stock_item|
#     Spree::StockMovement.create(quantity: 1, stock_item: stock_item)
#   end
# end

# # add dummy image to all products
# Spree::Product.find_each do |product|
#   file = open("https://i.imgur.com/08vatmr.png")
#   image = Spree::Image.create!(attachment: { io: file, filename: "#{rand(1..300)}.jpg" })
#   product.images << image
# end