default_store = Spree::Store.default
default_store.checkout_zone = Spree::Zone.find_by(name: 'Karnataka')
default_store.default_country = Spree::Country.find_by(iso: 'IN')
default_store.default_currency = 'INR'
default_store.supported_currencies = 'INR'
default_store.supported_locales = 'en'
# default_store.logo = 'logo.svg'
default_store.url = Rails.env.development? ? 'localhost:3000' : 'olivebee.co'
default_store.mail_from_address = 'support@olivbee.co'
default_store.name = 'OliveBee Organic Store'
default_store.save!

currencies = %w[INR]
Spree::Price.where(currency: 'INR').each do |price|
  currencies.each do |currency|
    new_price = Spree::Price.find_or_initialize_by(currency: currency, variant: price.variant)
    new_price.amount = price.amount
    new_price.save
  end
end

Spree::PaymentMethod.all.each do |payment_method|
  payment_method.stores = Spree::Store.all
end

if defined?(Spree::StoreProduct) && Spree::Product.method_defined?(:stores)
  Spree::Product.all.each do |product|
    product.store_ids = Spree::Store.ids
    product.save
  end
end