Spree::Sample.load_sample('option_types')

# color_option_type = Spree::OptionType.find_by!(name: 'color')
# length_option_type = Spree::OptionType.find_by!(name: 'length')
# size_option_type = Spree::OptionType.find_by!(name: 'size')
quantity_option_type = Spree::OptionType.find_by!(name: 'quantity')

# colors = {
#   white: '#FFFFFF',
#   purple: '#800080',
#   red: '#FF0000',
#   black: '#000000',
#   brown: '#8B4513',
#   green: '#228C22',
#   grey: '#808080',
#   orange: '#FF8800',
#   burgundy: '#A8003B',
#   beige: '#E1C699',
#   mint: '#AAF0D1',
#   blue: '#0000FF',
#   dark_blue: '#00008b',
#   khaki: '#BDB76B',
#   yellow: '#FFFF00',
#   light_blue: '#add8e6',
#   pink: '#FFA6C9',
#   lila: '#cf9de6',
#   ecru: '#F4F2D6'
# }

# lengths = { mini: 'Mini', midi: 'Midi', maxi: 'Maxi' }

# sizes = { xs: 'XS', s: 'S', m: 'M', l: 'L', xl: 'XL' }

quantity = {
  "20g": '20g',
  "25g": '25g',
  "30g": '30g',
  "35g": '35g',
  "40g": '40g',  
  "45g": '45g',
  "50g": '50g',
  "60g": '60g',
  "70g": '70g',
  "80g": '80g',
  "100g": '100g',
  "200g": '200g',
  "250g": '250g',
  "350g": '350g',
  "400g": '400g',
  "500g": '500g',
  "800g": '800g',  
  "1kg": '1kg', 
  "15kg": '1.5kg', 
  "1 bunch": '1 bunch', 
  '1 piece': '1 piece', 
  '2 piece': '2 piece', 
  '5 piece': '5 piece', 
  '12 piece': '12 piece', 
  '1 piece': '1 piece',
}

quantity.each_with_index do |q, index|
  quantity_option_type.option_values.find_or_create_by!(
    name: q.first,
    presentation: q.last,
    position: index + 1
  )
end


# colors.each_with_index do |color, index|
#   color_option_type.option_values.find_or_create_by!(
#     name: color.first,
#     presentation: color.last,
#     position: index + 1
#   )
# end

# lengths.each_with_index do |length, index|
#   length_option_type.option_values.find_or_create_by!(
#     name: length.first,
#     presentation: length.last,
#     position: index + 1
#   )
# end

# sizes.each_with_index do |size, index|
#   size_option_type.option_values.find_or_create_by!(
#     name: size.first,
#     presentation: size.last,
#     position: index + 1
#   )
# end
