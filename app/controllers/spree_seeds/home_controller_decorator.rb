module SpreeSeeds
    module Spree      
      module HomeControllerDecorator      
        def index
          @featured = ::Spree::Product.joins(:variants_including_master).limit(4)
        end
      end
    end
end
  
Spree::HomeController.prepend SpreeSeeds::Spree::HomeControllerDecorator