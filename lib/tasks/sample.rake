require "ffaker"
require "pathname"
require "spree/sample"

namespace :spree_seeds do
  
  desc "Loads seed data"
  task load: :environment do
    SpreeSample::Engine.load_samples
  end

  desc "Setup Store"
  task setup: :environment do
    SpreeSample::Engine.setup
  end

end
